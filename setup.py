import setuptools

setuptools.setup(
    name='debarchive-exporter',
    version='0',
    install_requires=[
        'prometheus_client',
        'werkzeug',
    ],
    packages=setuptools.find_namespace_packages(where='src'),
    package_dir={"": "src"},
    entry_points={
    },
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: POSIX :: Linux',
    ],
)
