# SPDX-License-Identifier: GPL-3.0-or-later

import typing
import pathlib

from datetime import datetime, timedelta, timezone

from .trace import Trace


class HealthApp:
    _traces: typing.List['HealthTrace']

    def __init__(self) -> None:
        self._traces = []

    def add_trace(self, path: pathlib.Path) -> None:
        self._traces.append(HealthTrace(path))

    def __call__(self, environ, start_response) -> typing.List[bytes]:
        status = '200 OK'
        ret = []

        for t in self._traces:
            t_healthy, t_text = t.check()

            if not t_healthy:
                status = '500 Unhealthy'

            ret.append(f'{t.path}: {t_text}\n'.encode('ascii'))

        start_response(status, [('content-type', 'text/plain')])
        return ret


class HealthTrace:
    path: pathlib.Path

    def __init__(self, path: pathlib.Path) -> None:
        self.path = path

    def check(self) -> typing.Tuple[bool, str]:
        try:
            with self.path.open('r') as f:
                trace = Trace(f)

        except Exception as ex:
            return False, str(ex)

        else:
            age = datetime.now(timezone.utc) - trace.date

            if age > timedelta(days=10):
                return False, f'Older then 10 days: {age}'

            return True, 'Existing'
