# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import pathlib
import socket

from prometheus_client import make_wsgi_app, REGISTRY  # type: ignore
try:
    from werkzeug.middleware.dispatcher import DispatcherMiddleware
except ImportError:
    from werkzeug.wsgi import DispatcherMiddleware  # type: ignore
from wsgiref.simple_server import WSGIServer, make_server

from .health import HealthApp
from .metrics import Collector, TraceCollector


parser = argparse.ArgumentParser(
)
parser.add_argument(
    '--mirrorname',
    default=socket.getfqdn(),
    help='set to the ftpsync config value of MIRRORNAME (default: %(default)s)',
    type=str,
)
parser.add_argument(
    'base',
    help='base path for all mirror archives',
    metavar='BASE',
    type=pathlib.Path,
)
parser.add_argument(
    'archives',
    default=['debian'],
    help='archive names (default: debian)',
    metavar='ARCHIVE',
    nargs='*',
    type=str,
)
parser.add_argument(
    '--port',
    default=9896,
    help='listen on port (default: %(default)s)',
    type=int,
)
args = parser.parse_args()

health_app = HealthApp()
collector = Collector()
REGISTRY.register(collector)
for archive in args.archives:
    path = args.base / archive / 'project' / 'trace' / args.mirrorname
    health_app.add_trace(path)
    collector.add_trace(TraceCollector(path, archive))


def main_app(environ, start_response):
    start_response('404 Not Found', [])
    return []


app = DispatcherMiddleware(main_app, {
    '/health': health_app,
    '/metrics': make_wsgi_app(),
})


class WSGIServerHack(WSGIServer):
    address_family = socket.AF_INET6


httpd = make_server('::', args.port, app, WSGIServerHack)
httpd.serve_forever()
