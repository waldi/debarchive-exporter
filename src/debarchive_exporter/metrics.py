# SPDX-License-Identifier: GPL-3.0-or-later

import typing
import pathlib

from datetime import datetime, timezone
from prometheus_client.core import GaugeMetricFamily, InfoMetricFamily  # type: ignore

from .trace import Trace


class Collector:
    _traces: typing.List['TraceCollector']

    def __init__(self) -> None:
        self._traces = []

    def add_trace(self, trace: 'TraceCollector') -> None:
        self._traces.append(trace)

    def collect(self) -> typing.Generator:
        metric_age = GaugeMetricFamily(
            'debarchive_mirror_age_seconds',
            'Time since last mirror update in seconds.',
            labels=['archive'],
        )

        metric_serial = GaugeMetricFamily(
            'debarchive_mirror_serial',
            'Latest archive serial of mirror',
            labels=['archive'],
        )

        metric_info = InfoMetricFamily(
            'debarchive_mirror',
            'Information about mirror',
            labels=['archive'],
        )

        metric_info_arch = InfoMetricFamily(
            'debarchive_mirror_arch',
            'Information about mirror architectures',
            labels=['archive'],
        )

        for t in self._traces:
            t.collect(metric_age, metric_serial, metric_info, metric_info_arch)

        yield metric_age
        yield metric_serial
        yield metric_info
        yield metric_info_arch


class TraceCollector:
    path: pathlib.Path
    archive: str

    def __init__(self, path: pathlib.Path, archive: str) -> None:
        self.path = path
        self.archive = archive

    def collect(
        self,
        metric_age: GaugeMetricFamily,
        metric_serial: GaugeMetricFamily,
        metric_info: InfoMetricFamily,
        metric_info_arch: InfoMetricFamily
    ) -> None:
        try:
            with self.path.open('r') as f:
                trace = Trace(f)

        except FileNotFoundError:
            pass

        else:
            age = datetime.now(timezone.utc) - trace.date

            metric_age.add_metric([self.archive], age.total_seconds())
            metric_serial.add_metric([self.archive], int(trace.archive_serial))
            metric_info.add_metric([self.archive], {
                'creator': trace.creator,
            })
            for arch in trace.architectures:
                metric_info_arch.add_metric([self.archive], {
                    'arch': arch,
                })
