# SPDX-License-Identifier: GPL-3.0-or-later

import email.utils
import typing

from datetime import datetime


class Trace:
    architectures: typing.Iterable[str]
    archive_serial: str
    creator: str
    date: datetime

    def __init__(self, trace: typing.TextIO):
        for line in trace:
            line_split = line.split(':', 1)
            if len(line_split) != 2:
                continue
            field = line_split[0].strip().lower()
            value = line_split[1].strip()

            if field == 'architectures':
                self.architectures = value.split()
            elif field == 'archive serial':
                self.archive_serial = value
            elif field == 'creator':
                self.creator = value
            elif field == 'date':
                self.date = email.utils.parsedate_to_datetime(value)
